import ray
import sys
import argparse
from ray import tune
from ray.rllib.agents.registry import get_agent_class
from ray.rllib.agents.ppo.ppo_policy_graph import PPOPolicyGraph
from ray.rllib.models import ModelCatalog
from ray.tune import run_experiments
from ray.tune.registry import register_env
import tensorflow as tf

from social_dilemmas.envs.harvest import HarvestEnv
from social_dilemmas.envs.cleanup import CleanupEnv
from models.conv_to_fc_net import ConvToFCNet


print ("for debugging ... Make sure to add /capitalism to your PYTHONPATH or simply '.'")
FLAGS = tf.app.flags.FLAGS

tf.app.flags.DEFINE_string(
    'exp_name', None,
    'Name of the ray_results experiment directory where results are stored.')
tf.app.flags.DEFINE_string(
    'env', 'cleanup',
    'Name of the environment to rollout. Can be cleanup or harvest.')
tf.app.flags.DEFINE_string(
    'algorithm', 'A3C',
    'Name of the rllib algorithm to use.')
tf.app.flags.DEFINE_integer(
    'num_agents', 5,
    'Number of agent policies')
tf.app.flags.DEFINE_integer(
    'train_batch_size', 30000,
    'Size of the total dataset over which one epoch is computed.')
tf.app.flags.DEFINE_integer(
    'checkpoint_frequency', 20,
    'Number of steps before a checkpoint is saved.')
tf.app.flags.DEFINE_integer(
    'training_iterations', 25000,
    'Total number of steps to train for')
tf.app.flags.DEFINE_integer(
    'num_cpus', 8,
    'Number of available CPUs')
tf.app.flags.DEFINE_integer(
    'num_gpus', 0,
    'Number of available GPUs')
tf.app.flags.DEFINE_boolean(
    'use_gpus_for_workers', False,
    'Set to true to run workers on GPUs rather than CPUs')
tf.app.flags.DEFINE_boolean(
    'use_gpu_for_driver', False,
    'Set to true to run driver on GPU rather than CPU.')
tf.app.flags.DEFINE_float(
    'num_workers_per_device', 2,
    'Number of workers to place on a single device (CPU or GPU)')

harvest_default_params = {
    'lr_init': 0.00136,
    'lr_final': 0.000028,
    'entropy_coeff': 0.000687}

cleanup_default_params = {
    'lr_init': 0.00126,
    'lr_final': 0.000012,
    'entropy_coeff': 0.00176}

def setup(env, hparams, algorithm, train_batch_size, num_cpus, num_gpus,
          num_agents, args, use_gpus_for_workers=False, use_gpu_for_driver=False,
          num_workers_per_device=1):

    # Calculate device configurations
    gpus_for_driver = int(use_gpu_for_driver)
    cpus_for_driver = 1 - gpus_for_driver
    if use_gpus_for_workers:
        spare_gpus = (num_gpus - gpus_for_driver)
        num_workers = int(spare_gpus * num_workers_per_device)
        num_gpus_per_worker = spare_gpus / num_workers
        num_cpus_per_worker = 0
        #Either it is this or num_gpus
        inc_count = num_gpus
    else:
        spare_cpus = (num_cpus - cpus_for_driver)
        num_workers = int(spare_cpus * num_workers_per_device)
        num_gpus_per_worker = 0
        num_cpus_per_worker = spare_cpus / num_workers
        #Either it is this or num_cpus
        inc_count = num_cpus

    if env == 'harvest':
        def env_creator(_):
            return HarvestEnv(num_agents=num_agents)
        single_env = HarvestEnv()
    else:
        def env_creator(_):
            return CleanupEnv(num_agents=args.num_agents, paradigm=args.paradigm, pre_train_size=args.pre_train_size, 
                            apple_prob=args.apple_prob, waste_prob=args.waste_prob, beta=args.beta, epsillon=args.epsillon,
                            window_size=args.window_size, discount_rate=args.discount_rate, alpha=args.alpha,
                            num_cleaners=args.cleaners, num_harvesters=args.harvesters, inc_count=inc_count)
        single_env = CleanupEnv(num_agents=args.num_agents, paradigm=args.paradigm, pre_train_size=args.pre_train_size,
                            apple_prob=args.apple_prob, waste_prob=args.waste_prob, beta=args.beta, epsillon=args.epsillon,
                            window_size=args.window_size, discount_rate=args.discount_rate, alpha=args.alpha,
                            num_cleaners=args.cleaners, num_harvesters=args.harvesters, inc_count=inc_count)

    env_name = env + "_env"
    register_env(env_name, env_creator)

    obs_space = single_env.observation_space
    act_space = single_env.action_space

    # Each policy can have a different configuration (including custom model)
    def gen_policy():
        return (PPOPolicyGraph, obs_space, act_space, {})

    # Setup PPO with an ensemble of `num_policies` different policy graphs
    policy_graphs = {}
    for i in range(num_agents):
        policy_graphs['agent-' + str(i)] = gen_policy()

    def policy_mapping_fn(agent_id):
        return agent_id

    # register the custom model
    model_name = "conv_to_fc_net"
    ModelCatalog.register_custom_model(model_name, ConvToFCNet)

    agent_cls = get_agent_class(algorithm)
    config = agent_cls._default_config.copy()

    # information for replay
    config['env_config']['func_create'] = tune.function(env_creator)
    config['env_config']['env_name'] = env_name
    config['env_config']['run'] = algorithm

    # hyperparams
    config.update({
                "train_batch_size": train_batch_size,
                "horizon": 1000,
                "lr_schedule":
                [[0, hparams['lr_init']],
                    [20000000, hparams['lr_final']]],
                "num_workers": num_workers,
                "num_gpus": gpus_for_driver,  # The number of GPUs for the driver
                "num_cpus_for_driver": cpus_for_driver,
                "num_gpus_per_worker": num_gpus_per_worker,   # Can be a fraction
                "num_cpus_per_worker": num_cpus_per_worker,   # Can be a fraction
                "entropy_coeff": hparams['entropy_coeff'],
                "multiagent": {
                    "policy_graphs": policy_graphs,
                    "policy_mapping_fn": tune.function(policy_mapping_fn),
                },
                "model": {"custom_model": "conv_to_fc_net", "use_lstm": True,
                          "lstm_cell_size": 128}

    })
    return algorithm, env_name, config

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--paradigm', type=int, default=1, help='-1 - Democracy 0 - Communism 1 - Capitalism 2 - Pre-train')
    parser.add_argument('--num-agents', type=int, default=5, help='No of agents in the setup')
    parser.add_argument('--pre-train-size', type=int, default=2000, help='Pre-training iterations')
    parser.add_argument('--apple-prob', type=float, default=0.3, help='Apple respawn probability')
    parser.add_argument('--waste-prob', type=float, default=0.3, help='Waste spawn probability')
    parser.add_argument('--beta', type=float, default=0.75, help='Artifical reward for main action during pre-training')
    parser.add_argument('--epsillon', type=float, default=0.25, help='Artifical reward for secondary action during pre-training')
    parser.add_argument('--window-size', type=int, default=100, help='Window size to consider while calculate the work done')
    parser.add_argument('--discount-rate', type=float, default=0.9, help='Discount factor for calulating rewards')
    parser.add_argument('--alpha', type=float, default=0.5, help='Redistibution ratio of rewards')
    parser.add_argument('--cleaners', type=int, default=3, help='No of cleaners for pre-training phase')
    parser.add_argument('--harvesters', type=int, default=2, help='No of harvesters for pre-training phase')
    return parser.parse_args()

def main(unused_argv):
    args = parse_args()
    if args.cleaners + args.harvesters != args.num_agents :
        print ('Cleaners + Harvesters not equal to total agents... Quitting')
        exit(0)
    ray.init(num_cpus=FLAGS.num_cpus, object_store_memory=int(8.9e8))
    if FLAGS.env == 'harvest':
        hparams = harvest_default_params
    else:
        hparams = cleanup_default_params
    alg_run, env_name, config = setup(FLAGS.env, hparams, FLAGS.algorithm,
                                      FLAGS.train_batch_size,
                                      FLAGS.num_cpus,
                                      FLAGS.num_gpus, FLAGS.num_agents,
                                      args,
                                      FLAGS.use_gpus_for_workers,
                                      FLAGS.use_gpu_for_driver,
                                      FLAGS.num_workers_per_device)

    if FLAGS.exp_name is None:
        exp_name = FLAGS.env + '_' + FLAGS.algorithm
    else:
        exp_name = FLAGS.exp_name
    print('Commencing experiment', exp_name)

    run_experiments({
        exp_name: {
            "run": alg_run,
            "env": env_name,
            "stop": {
                "training_iteration": FLAGS.training_iterations
            },
            'checkpoint_freq': FLAGS.checkpoint_frequency,
            'max_failures': int(1e5),
            "config": config,
        }
    })


if __name__ == '__main__':
    tf.app.run(main)
