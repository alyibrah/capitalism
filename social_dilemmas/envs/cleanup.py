import numpy as np
import random

from social_dilemmas.constants import CLEANUP_MAP
from social_dilemmas.envs.map_env import MapEnv, ACTIONS
from social_dilemmas.envs.agent import CleanupAgent  # CLEANUP_VIEW_SIZE

# Add custom actions to the agent
ACTIONS['FIRE'] = 5  # length of firing beam
ACTIONS['CLEAN'] = 5  # length of cleanup beam

# Custom colour dictionary
CLEANUP_COLORS = {'C': [100, 255, 255],  # Cyan cleaning beam
                  'S': [113, 75, 24],  # Light grey-blue stream cell
                  'H': [99, 156, 194],  # brown waste cells
                  'R': [113, 75, 24]}  # Light grey-blue river cell

SPAWN_PROB = [0, 0.005, 0.02, 0.05]

thresholdDepletion = 0.4
thresholdRestoration = 0.0

appleRespawnProbability = 0.05
wasteSpawnProbability = 0.5
# TODO Change these values appropriately
# TODO keep the option to enable separate spawn rates for the pretraining phase
appleRespawnProbability_pretrain = 0.3
wasteSpawnProbability_pretrain = 0.3

COMMUNISM = 0
CAPITALISM = 1
PRE_TRAIN = 2
CLEANER = 0
HARVESTER = 1
FLEXIBLE = -1


class CleanupEnv(MapEnv):

    def __init__(self, ascii_map=CLEANUP_MAP, num_agents=1, render=False, paradigm=COMMUNISM, pre_train_size=200, apple_prob=0.05, waste_prob=0.5, beta=0.9, epsillon=0.1, 
                window_size=1000, discount_rate=0.9, alpha=0.5, num_cleaners=3, num_harvesters=2, inc_count=2):
        super().__init__(ascii_map=ascii_map, num_agents=num_agents, render=render, paradigm=paradigm, pre_train_size=pre_train_size, beta=beta, epsillon=epsillon, 
                    window_size=window_size, discount_rate=discount_rate, alpha=alpha, num_cleaners=num_cleaners, num_harvesters=num_harvesters)
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>CLEANUP PARADIGM ", paradigm)
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>CLEANUP PRE_TRAIN_SIZE ", pre_train_size)
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>BETA ", beta)
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>EPSILLON ", epsillon)
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>WINDOW SIZE ", window_size)
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>DISCOUNT RATE ", discount_rate)
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>ALPHA ", alpha)
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>NO OF CLEANERS ", num_cleaners)
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>NO OF HARVESTERS ", num_harvesters)
        # compute potential waste area
        unique, counts = np.unique(self.base_map, return_counts=True)
        counts_dict = dict(zip(unique, counts))
        self.inc_count = inc_count
        self.switch_to_train = 0
        self.potential_waste_area = counts_dict.get('H', 0) + counts_dict.get('R', 0)
        if paradigm == PRE_TRAIN:
            self.current_apple_spawn_prob = apple_prob
            self.current_waste_spawn_prob = waste_prob 
            self.appleRespawnProbability = apple_prob 
            self.wasteSpawnProbability = waste_prob
        else:
            self.current_apple_spawn_prob = apple_prob
            self.current_waste_spawn_prob = waste_prob 
            self.appleRespawnProbability = apple_prob 
            self.wasteSpawnProbability = waste_prob
        print ("APPLE PROB", self.appleRespawnProbability)
        print ("WASTE PROB", self.wasteSpawnProbability)
        self.compute_probabilities()

        # make a list of the potential apple and waste spawn points
        self.apple_points = []
        self.waste_start_points = []
        self.waste_points = []
        self.river_points = []
        self.stream_points = []
        for row in range(self.base_map.shape[0]):
            for col in range(self.base_map.shape[1]):
                if self.base_map[row, col] == 'P':
                    self.spawn_points.append([row, col])
                elif self.base_map[row, col] == 'B':
                    self.apple_points.append([row, col])
                elif self.base_map[row, col] == 'S':
                    self.stream_points.append([row, col])
                if self.base_map[row, col] == 'H':
                    self.waste_start_points.append([row, col])
                if self.base_map[row, col] == 'H' or self.base_map[row, col] == 'R':
                    self.waste_points.append([row, col])
                if self.base_map[row, col] == 'R':
                    self.river_points.append([row, col])
                # Removed this as ghost agent does the cleaning
                """
                if self.pre_train_method == PRE_TRAIN_HARVEST:
                    self.waste_start_points = []
                    self.waste_points = []
                """
        self.color_map.update(CLEANUP_COLORS)
        self.iteration_idx = 0

    @property
    def action_space(self):
        agents = list(self.agents.values())
        return agents[0].action_space

    @property
    def observation_space(self):
        # FIXME(ev) this is an information leak
        agents = list(self.agents.values())
        return agents[0].observation_space

    def custom_reset(self):
        print("Iteration idx = " + str(self.iteration_idx))
        """Initialize the walls and the waste"""
        for waste_start_point in self.waste_start_points:
            self.world_map[waste_start_point[0], waste_start_point[1]] = 'H'
        for river_point in self.river_points:
            self.world_map[river_point[0], river_point[1]] = 'R'
        for stream_point in self.stream_points:
            self.world_map[stream_point[0], stream_point[1]] = 'S'
        self.compute_probabilities()

        # TODO Anirudh changes for pre-training based on iteration count (iteration_idx) should be incremented by number
        # TODO      of gpus or workers or whatever, and we might be able to pass that from the train_baseline.py
        #print (">>>>>>>>>>>>>>>>>Iteration idx = ", self.iteration_idx)
        #print (">>>>>>>>>>>>>>>>>Pretrain size= ", self.pre_train_size)
        self.iteration_idx += self.inc_count

        #print (">>>>>>>>>>>>>>>>>PARADIGM = ", self.paradigm)
        if self.paradigm == PRE_TRAIN and self.iteration_idx >= self.pre_train_size and self.switch_to_train == 0:
            self.paradigm = CAPITALISM
            for i in range(self.num_agents):
                agent_id = 'agent-' + str(i)
                self.agents[agent_id].set_agent_type(FLEXIBLE)
            
                print ("Agent id", agent_id, "Agent Type", self.agents[agent_id].agent_type)
            self.switch_to_train = 1
            print ('#######################MADE THE SWITCH############################')

    def custom_action(self, agent, action):
        """Allows agents to take actions that are not move or turn"""
        updates = []
        if action == 'FIRE':
            agent.fire_beam('F')
            updates = self.update_map_fire(agent.get_pos().tolist(),
                                           agent.get_orientation(), ACTIONS['FIRE'],
                                           fire_char='F', firing_agent_id=agent.agent_id)
        elif action == 'CLEAN':
            agent.fire_beam('C')
            updates = self.update_map_fire(agent.get_pos().tolist(),
                                           agent.get_orientation(),
                                           ACTIONS['FIRE'],
                                           fire_char='C',
                                           cell_types=['H'],
                                           update_char=['R'],
                                           blocking_cells=['H'],
                                           firing_agent_id=agent.agent_id)
        return updates

    def custom_map_update(self):
        """"Update the probabilities and then spawn"""
        self.compute_probabilities()
        self.update_map(self.spawn_apples_and_waste())

    def setup_agents(self):
        """Constructs all the agents in self.agent"""
        map_with_agents = self.get_map_with_agents()
        agent_type = FLEXIBLE
        cleaners = self.num_cleaners
        harvesters = self.num_harvesters
        for i in range(self.num_agents):
            agent_id = 'agent-' + str(i)
            if self.paradigm == PRE_TRAIN and cleaners > 0:
                agent_type = CLEANER
                cleaners -= 1    
            elif self.paradigm == PRE_TRAIN and harvesters > 0:
                agent_type = HARVESTER
                harvesters -= 1
            # TODO Add Code for loading previous pre-trained configurations
            spawn_point = self.spawn_point()
            rotation = self.spawn_rotation()
            # grid = util.return_view(map_with_agents, spawn_point,
            #                         CLEANUP_VIEW_SIZE, CLEANUP_VIEW_SIZE)
            # agent = CleanupAgent(agent_id, spawn_point, rotation, grid)
            agent = CleanupAgent(agent_id, spawn_point, rotation, map_with_agents, agent_type=agent_type, beta=self.beta, epsillon=self.epsillon, window_size=self.window_size)
            self.agents[agent_id] = agent

    def spawn_apples_and_waste(self):
        spawn_points = []
        # spawn apples, multiple can spawn per step
        for i in range(len(self.apple_points)):
            row, col = self.apple_points[i]
            # don't spawn apples where agents already are
            if [row, col] not in self.agent_pos and self.world_map[row, col] != 'A':
                rand_num = np.random.rand(1)[0]
                if rand_num < self.current_apple_spawn_prob:
                    spawn_points.append((row, col, 'A'))

        # spawn one waste point, only one can spawn per step
        if not np.isclose(self.current_waste_spawn_prob, 0):
            random.shuffle(self.waste_points)
            for i in range(len(self.waste_points)):
                row, col = self.waste_points[i]
                # don't spawn waste where it already is
                if self.world_map[row, col] != 'H':
                    rand_num = np.random.rand(1)[0]
                    if rand_num < self.current_waste_spawn_prob:
                        spawn_points.append((row, col, 'H'))
                        break
        return spawn_points

    def compute_probabilities(self):
        waste_density = 0
        if self.potential_waste_area > 0:
            waste_density = 1 - self.compute_permitted_area() / self.potential_waste_area
        if waste_density >= thresholdDepletion:
            self.current_apple_spawn_prob = 0
            self.current_waste_spawn_prob = 0
        else:
            self.current_waste_spawn_prob = self.wasteSpawnProbability
            if waste_density <= thresholdRestoration:
                self.current_apple_spawn_prob = self.appleRespawnProbability
            else:
                spawn_prob = (1 - (waste_density - thresholdRestoration)
                              / (thresholdDepletion - thresholdRestoration)) \
                             * self.appleRespawnProbability
                self.current_apple_spawn_prob = spawn_prob

    def compute_permitted_area(self):
        """How many cells can we spawn waste on?"""
        unique, counts = np.unique(self.world_map, return_counts=True)
        counts_dict = dict(zip(unique, counts))
        current_area = counts_dict.get('H', 0)
        free_area = self.potential_waste_area - current_area
        return free_area
